Versão 1.0.1
- Adicionado ícone png
- Adicionado vhist.txt (histórico)


Versão 1.0.2
- Adicionado css
- Adicionado mensagens de erros
- Alteração da estrutura de arquivos (css/js)
- Adicionado função de enviar link com a tecla "enter"


Versão 1.0.3
- Retirada mensagem de erro (bug) forçada


Versão 1.0.4
- Compactado (.zip)


Versão 1.0.5
- Alterado o tamano da popup inicial


Versão 1.0.6
- Alterado arquivo jquery.min para resolver problema de min.map
