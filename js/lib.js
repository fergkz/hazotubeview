function getURLParameter(name, string) {

    search = string ? string : location.search;

    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(search)||[,null])[1]
    );
}