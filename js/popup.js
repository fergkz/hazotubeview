function showerror(error){
    $('.error').remove();
    
    randomNum = Math.round( Math.random() * 121497313216476 );
    
    text = "<div class='error "+randomNum+"'>"+error+"</div>";
    
    $('td.btn').append(text);
    
    setTimeout(function(){
        $('.'+randomNum).remove();
    }, 2500);
    
}

function sendform(){

    content = jQuery('#link').val();

    v = getURLParameter('v', content);

    if( v === null || v === undefined || v === '' || v === 'null' ){
        showerror("url inv&aacute;lida");
    }else{
        bgPage.open(content);
        window.close();
    }
}
    
$().ready(function() {
    
    bgPage = chrome.extension.getBackgroundPage();

    $('#go').on('click', function() { 
        sendform();
    });

    $("#link").keypress(function(event) {
        if( event.which === 13 ){
            sendform();
        }
    });

});